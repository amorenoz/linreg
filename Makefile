CC = gcc
CFLAGS = -g -Wall -Wextra -Isrc -Werror
LDLIBS = -lm

SOURCES = $(wildcard src/**/*.c src/*.c) 
OBJECTS = $(patsubst %.c,%.o,$(SOURCES))

HEADERS = $(wildcard src/**/*.h src/*.h)

TEST_SRC = $(wildcard test/*_test.c)
TESTS = $(patsubst %.c,%,$(TEST_SRC))

TARGET = build/liblinreg.a

default : $(TARGET)

all: $(TARGET) tests

$(TARGET): build $(OBJECTS)
	ar rcs $@ $(OBJECTS)
	ranlib $@

$(TESTS): $(TARGET) $(HEADERS)

$(OBJECTS) : $(SOURCES) $(HEADERS)
#	$(CC) -c $< -o $@ $(CFLAGS)



build:
	@mkdir -p build

# Tests
.PHONY: tests
tests: CFLAGS += $(TARGET)
tests: $(TESTS)
	chmod +x $(TESTS)
	./runtests

valgrind:
	VALGRIND="valgrind --log-file=/tmp/valgrind-%p.log" $(MAKE)
	
clean:
	rm -f $(OBJECTS) $(TESTS)
	rm -f $(TARGET)

