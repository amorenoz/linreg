#include "error.h"
//#include "../src/matrix.h"
#include "matrix.h"
#include "lineq.h"

#include <math.h>
#include <stdio.h>

int main(void)
{
    printf("Testing linear equations solvers\n");
    double array[3][3] = {
          { 1, 3, 5 },
          { 2, 4, 7 },
          { 1, 1, 0 }
          };
    
    matrix_t* A = mat_array(3, 3, array);

    double soln[3][1] = {
      {1},
      {2},
      {3}
      };

    double ans[3][1] = { //Solution
      {1.5},
      {1.5},
      {-1.0}
    };
      
    matrix_t* b = mat_array(3, 1, soln);
    matrix_t* x = mat_array(3, 1, soln);

    lin_solve(A, b, x, LUNP);
    mat_print_name(x, "x");

    matrix_t* answer = mat_array(3, 1, ans);

    int rc;
    if(!mat_equal(x,answer)) {
      printf("Linear solver is not working.\n");
      rc = 1;
    } else {
      printf("Linear solver test PASSED.\n");
      rc = 0;      
    }
    mat_free(A);
    mat_free(x);
    mat_free(b);
    mat_free(answer);
    return rc;

}
