#include "private_matrix.h"
#include "error.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define MATSIZE(m) ((matrix_t *) m)->rows * ((matrix_t *) m)->cols * sizeof(TYPE)


// ----- Element Getters / Setters ------
TYPE mat_get(const matrix_t * m, const size_t i, const size_t j)
{
    if (i >= m->rows) {
        ERROR("Row index out of range");
    }
    if (j >= m->cols) {
        ERROR("Column index out of range");
    }
    // Transpose Matrixes
    if (m->flags & M_TRANSPOSE)
        return block_get(m->block, (j * m->rows + i));  // Submatrix and transpose not implemented
    else if (m->flags & M_SUBMATRIX)
        return block_get(m->block, ((i + m->sub_rows) * m->master_cols + (j + m->sub_cols)));
    else
        return block_get(m->block, (i * m->cols + j));
}

void mat_set(const matrix_t * m, const size_t i, const size_t j, const TYPE value) 
{
    if (i >= m->rows) {
        ERROR("Row index out of range");
    }
    if (j >= m->cols) {
        ERROR("Column index out of range");
    }
    if (m->flags & M_TRANSPOSE)
        block_set(m->block, (j * m->rows + i), value);
    else if (m->flags & M_SUBMATRIX)
        block_set(m->block, ((i + m->sub_rows) * m->master_cols + (j + m->sub_cols)), value);
    else
        block_set(m->block, (i * m->cols + j), value);
}

// If we wanted to expose the content of matrix_t, this would be inline!
size_t mat_row(const matrix_t * m) { return m->rows; }
size_t mat_col(const matrix_t * m) { return m->cols; }

void mat_setsize(matrix_t * m, const size_t rows, const size_t cols)
{
    m->rows = rows;
    m->cols = cols;
    m->sub_rows = 0;
    m->sub_cols = 0;
}

void mat_insert_row(matrix_t *  m, const matrix_t * row, const size_t index)
{
    size_t pos = m->rows - 1;
    size_t cols;
    if (index >= m->rows) {
        ERROR("Row index insertion out of range");
    }
    if (row->cols != m->cols) {
        ERROR("Cannot insert row in matrix with different number of columns");
    }
     
    while (pos != 0 && pos != index) {
        for (cols = 0; cols < m->cols; cols ++) {
            mat_set(m, pos, cols, mat_get(m, pos - 1, cols));
        }
        pos--;
    }
    for (cols = 0; cols < row->cols; cols ++) {
        mat_set(m, pos, cols, mat_get(row, 0, cols));
    }
} 

void mat_insert_col(matrix_t *  m, const matrix_t * col, const size_t index)
{
    size_t pos = m->cols - 1;
    size_t rows;
    if (index >= m->cols) {
        ERROR("Column index insertion out of range");
    }
    if (col->rows != m->rows) {
        ERROR("Cannot insert column in matrix with different number of rows");
    }
     
    while (pos != 0 && pos != index) {
        for (rows = 0; rows < m->rows; rows ++) {
            mat_set(m, rows, pos, mat_get(m, rows, pos - 1));
        }
        pos--;
    }
    for (rows = 0; rows < col->rows; rows ++) {
        mat_set(m, rows, pos, mat_get(col, rows, 0));
    }
} 
//  ----- Matrix Constructors ----
// ----- Alloc / free -----
matrix_t * mat_alloc(const size_t rows, const size_t cols)
{
    matrix_t * m;

    if (rows == 0) {
        ERROR ("Rows must be greater then 0");
    }
    else if (cols == 0) {
        ERROR("columns must be greater than 0");
    }

    m = malloc(sizeof(matrix_t));
    if (m == NULL) {
        ERROR("Failed to allocate matrix structure");
    }
    m->block = 0;
    m->flags = 0;
    mat_setsize(m, rows, cols);
         
    return m;
}

matrix_t * mat_ROsubmatrix(const matrix_t * m,
                              const size_t row_from, 
                              const size_t row_to,
                              const size_t col_from, 
                              const size_t col_to)
{
    size_t nrows = row_to - row_from + 1;
    size_t ncols = col_to - col_from + 1; 

    matrix_t * sub_m = mat_create(nrows, ncols);
    if (m == NULL) {
        ERROR("Failed to create matrix");
    }
     
    mat_ROsubmatrix_to(m, sub_m, row_from, row_to, col_from, col_to);
    return sub_m;
}


void mat_ROsubmatrix_to(const matrix_t * m,
                              matrix_t * sub_m,
                              const size_t row_from, 
                              const size_t row_to,
                              const size_t col_from, 
                              const size_t col_to)
{
    size_t nrows = row_to - row_from + 1;
    size_t ncols = col_to - col_from + 1; 
    if (row_from > m->rows || row_to > m->rows || col_from > m->rows || col_to > m->cols) {
        ERROR("Submatrix out of range");
    }
    if (row_from > row_to || col_from > col_to)
        ERROR("Incorrect range");
    
    // Freeing the original block
    block_rem_ref(sub_m->block);

    mat_setsize(sub_m, nrows, ncols);
      
    block_t * block = block_add_ref(m->block);
    if (block == NULL) {
        ERROR("Failed to assign block");
    }
    
    sub_m->flags |= M_SUBMATRIX;
    sub_m->sub_rows = row_from; 
    sub_m->sub_cols = col_from;
    sub_m->master_cols = m->cols;
    sub_m->block = block;

}
void mat_resize(matrix_t * m, const size_t rows, const size_t cols)
{
    if (rows *  cols < m->rows * m->cols) {
//        printf("Warning: reducing size of matrix. DATA WILL BE LOST\n");
    }
    block_realloc(m->block, rows * cols);
    mat_setsize(m, rows, cols);
}

void mat_free(matrix_t * m)
{
    // Free (or not) the memory block
    block_rem_ref(m->block);
    // Free matrix structure
    free(m);
}

// Create new matrix
matrix_t * mat_create(const size_t rows, const size_t cols)
{
    matrix_t * m = mat_alloc(rows, cols);
    if (m == NULL) {
        ERROR("Failed to allocate matrix");
    }
    block_t * block = block_alloc(sizeof(TYPE) * (rows * cols));
    if (block == NULL) {
        ERROR("Failed to allocate space for the Matrix data");
    }
    m->block = block;
    return m;
}

// Create new matrix with the same content as the given one
matrix_t * mat_copy(const matrix_t * other) 
{
    matrix_t * m = mat_alloc(other->rows, other->cols);
    if (m == NULL) {
        ERROR("Failed to allocate matrix");
    }

    block_t * block = block_copy(other->block);
    if (block == NULL) {
        ERROR("Failed to copy block");
    }
    m->block = block; 
    return m;
}

// Create new matrix structure sharing the data with the given one
matrix_t * mat_assign_to(const matrix_t * other)
{
    matrix_t * m = mat_alloc(other->rows, other->cols);
    if (m == NULL) {
        ERROR("Failed to allocate matrix");
    }

    block_t * block = block_add_ref(other->block);
    if (block == NULL) {
        ERROR("Failed to assign block");
    }
    m->block = block;
    return m;
}
/*
 * Read only transpose. It will access the same data as the 
 * original matrix. So if you try to change it, the original will also 
 * be changed.
*/
matrix_t * mat_ROtranspose(const matrix_t * other)
{
    matrix_t * mtrans = mat_assign_to(other);
    mat_setsize(mtrans, other->cols, other->rows);
    mtrans->flags |= M_TRANSPOSE;
    return mtrans;
}

matrix_t * mat_transpose(const matrix_t * other)
{
    matrix_t * m = mat_create(other->rows, other->cols);
    if (m == NULL) {
        ERROR("Failed to create matrix");
    }
    size_t r,c; 
    for (r = 0; r < m->rows; r++) {
        for (c = 0; c < m->cols; c++) {
            mat_set(m, r, c, mat_get(other, c, r));
        }
    }
    return m;
}

// This is not portable: C99 Only
matrix_t * mat_array(const size_t rows, const size_t cols, TYPE array[rows][cols])
{
    size_t r, c; 
    matrix_t * m;
    m = mat_create(rows, cols);
    
    for (r = 0; r < m->rows; r++) {
        for (c = 0; c < m->cols; c++) {
            mat_set(m, r, c, array[r][c]);
        }
    }
    return m;
}

matrix_t * mat_zero(const size_t rows, const size_t cols)
{
    size_t r,c;
    matrix_t * m = mat_create(rows, cols);
    if (m == NULL) {
        ERROR("Failed to allocate matrix");
    }
    for (r = 0; r < rows; r++) {
        for (c = 0; c < cols; c++) {
            mat_set(m, r, c, (TYPE) 0);
        }
    }
    return m;
}

matrix_t * mat_eye(const size_t size)
{
    size_t r,c;
    matrix_t * m = mat_create(size, size);
    if (m == NULL) {
        ERROR("Failed to allocate matrix");
    }
    for (r = 0; r < size; r++) {
        for (c = 0; c < size; c++) {
            if (r == c) {
                mat_set(m, r, c, (TYPE) 1);
            } else {
                mat_set(m, r, c, (TYPE) 0);
            }
        }
    }
    return m;
}


// ----- Printing helpers -----
void mat_print_name(const matrix_t * m, const char * string)
{
    printf("%s [%u] [%u] = \n", string, (unsigned int) m->rows, (unsigned int) m->cols);
    mat_print(m);
}

void mat_print(const matrix_t * m)
{
    size_t r,c;
    if (m == NULL) {
        printf("matrix not allocated\n");
        return;
    }
    for (r = 0; r < m->rows; r++) {
        for (c = 0; c < m->cols; c++) {
            printf(" %5.4f  ", mat_get(m, r, c));
        }
        printf("\n");
    }
}
