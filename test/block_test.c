
#include "matrix.h"
#include "error.h"

int main() {
    
    // Allocate 100 TYPEs
    size_t N = 100;
    block_t* b1 = block_alloc(N * sizeof(TYPE));
    size_t i;
    for (i = 0; i < N; i++) {
        (b1->data)[i] = (TYPE)i;
    }
    
    block_t* b2 = block_add_ref(b1);
    
    if (b2 != b1) {
        ERROR("Assigned blocks are not equal");
    }
    for (i = 0; i < N; i++) {
        if((b2->data)[i] != (TYPE)i) {
            ERROR("Assigned blocks do not have the same content");
        } 
    }
    
    block_t*  b3 = block_add_ref(b2); 
    for (i = 0; i < N; i++) {
        if((b3->data)[i] != (TYPE)i) {
            ERROR("Assigned blocks do not have the same content");
        } 
    }
    if (b3 != b1) {
        ERROR("Assigned blocks are not equal");
    }
     
    block_rem_ref(b1); 
    TYPE sum;
    fflush(stdout);
    // See if we segfault...
    for (i = 0; i < N; i++) {
        sum+=(b2->data)[i];
    } 
    block_rem_ref(b2);
    fflush(stdout);
    // See if we segfault...
    for (i = 0; i < N; i++) {
        sum+=(b2->data)[i];
    }
    printf("Removing last ref\n");
    fflush(stdout);
    block_rem_ref(b3);
    
    size_t nbytes = N * sizeof(TYPE);
    block_t * b = block_alloc(nbytes);
    for (i = 0; i < N; i++) {
        (b->data)[i] = (TYPE)i;
    }
    block_t * bcpy = block_copy(b);
    for (i = 0; i < N; i++) {
        if (b->data[i] != bcpy->data[i])
            ERROR("Copyed blocks don't match");

    }
    block_rem_ref(b);
    block_rem_ref(bcpy);
    return 0;
}
