#include "timing.h" 
#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#define MAX_TIMESTAMP 20
#define TIME_FORMAT "%hu-%02hu-%02hu %02hu:%02hu"

/*
  Convert to/from mins from epoch 
*/
mytime_t get_time(const unsigned short year, 
                const unsigned short month,
                const unsigned short day,
                const unsigned short hour,
                const unsigned short min)
{
return (mytime_t) (( (year - 1970) << 21) + (month << 17) + (day << 12) + (hour << 6) + min);
}

/*
  Convert mytime_t into string
*/
void time_string(const mytime_t time, char* time_str)
{
    unsigned short year, month, day, hour, min;
    year = (((time) & 0xFFF00000) >> 21) + 1970;
    month = (time & 0xE0000) >> 17;
    day = (time & 0x1F000) >> 12;
    hour = (time & 0xFC0) >> 6;
    min = (time & 0x3F);

    snprintf(time_str, MAX_TIMESTAMP, TIME_FORMAT, year, month, day, hour, min); 
}

/*
  Format of timestamp is: 2008-01-05 18:00
*/
mytime_t parse_time(const char* time_str)
{
    unsigned short year, month, day, hour, min;
    if (sscanf(time_str, TIME_FORMAT, &year, &month, &day, &hour, &min) != 5) {
        printf("Error parsing timestamp: %s", time_str);
        return -1;
    }
    return get_time(year, month, day, hour, min); 
}


/*
 Measure the time of a given function
*/
int time_fn(
        int (*fp)(),         // function to measure
        measure_t * measure) // Measurement placeholder
{
    clock_t c_start, c_end;
    time_t t_start, t_end;
    c_start = clock();
    t_start = time(NULL);
    
    int result = (fp)();

    c_end = clock();
    t_end = time(NULL);

    measure->time = t_end - t_start;
    double time =  (c_end - c_start) / (double)CLOCKS_PER_SEC;
    measure->clock = time;
    return result;
}

void print_measure(const char* description, const measure_t * m)
{
    printf ("%s: Clock time: %g s. Total time: %u s \n",
            description, m->clock, m->time);
}

