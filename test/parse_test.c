#include "parse.h"
#include "probs.h"
#include "timing.h"

#include <stdio.h>

int time_parsing()
{
    char* time = "2008-01-02 12:00";
    mytime_t timestamp = parse_time(time);
    printf("Timestamp conversion: %s  ->  %lu \n", time, timestamp);
    // Inverse conversion
    char time_str[MAX_TIMESTAMP];
    time_string(timestamp, time_str); 
    printf("Inverse conversion: %lu  ->  %s \n", timestamp, time_str);
    return 1;
}

int main()
{
    time_parsing();
    
    linreg_prob_t prob;
    if (parse_file("doc/datos_problema.txt", &prob) != 1)
        return -1;
    //mat_print_name(prob.pred_timestamp, "Pred time");
    mat_print_name(prob.predict_X, "PredX");
    matrix_t * A = mat_assign_to(prob.IV);
    matrix_t * At = mat_ROtranspose(A);
    matrix_t * AAt = mat_mult(A, At);
   // mat_print_name(AAt, "AAt"); You really don't want to see it
    mat_free(AAt);
    mat_free(A);
     
    return 0;
}
