#include "private_matrix.h"

#include "error.h"

#include <math.h>

#define PRECISION 0.0000001

// Matrix operations

// Matrix comparison
int mat_equal(matrix_t * m, matrix_t * p)
{
    size_t r, c;
    if (m->rows != p->rows || m->cols != p->cols)
        return 0;

    for (r = 0; r < m->rows; r++) {
        for (c = 0; c < m->cols; c++) {
           if (abs(mat_get(m, r, c) - mat_get(p, r, c)) > PRECISION)
               return 0;
        }
    }
    return 1;
}

/*
 Matrix Multiplication:
 Multiplies matrixes A * B and stores the result in the empty matrix C

 requires
      dimensions of A and B match: A.cols == B.rows
      Enough space has already been allocated for C (will not malloc())
 returns
      C sized A.rows x B.cols 
*/
void mat_mult_to(const matrix_t * A, const matrix_t * B, matrix_t * C)
{
    size_t r,c,i;

    if (A->cols != B->rows) {
        ERROR("Matrix dimansion missmatch");
    }
    TYPE sum = 0;
    C->rows = A->rows;
    C->cols = B->cols;
    for (r = 0; r < C->rows; r++) {
        for (c = 0; c < C->cols; c++) {
            for (i = 0; i < A->cols; i++) {
                sum += mat_get(A, r, i) * mat_get(B, i, c);
            }
            mat_set(C, r, c, sum);
            sum = 0;
        }
    }
}
/*
  Multiplication creation helper
  Creates a new matrix and initializes it as the multiplication of A * B
 
  returns
  C sized A.rows x B.cols 
*/
matrix_t * mat_mult(const matrix_t * A, const matrix_t * B)
{
    matrix_t * C = mat_create(A->rows, B->cols);
    mat_mult_to(A, B, C);
    return C;
}

/*
 Matrix Addition:
 Sums matrixes A + B and stores the result in matrix C

 requires
      dimensions of A and B match: A.rows == B.rows && A.cols == B.cols
      Enough space has already been allocated for C (will not malloc())
*/
void mat_sum(const matrix_t * A, const matrix_t * B, matrix_t * C)
{
    size_t r,c;
    if (A->rows != B->rows || A->cols != B->cols) {
        ERROR("Matrix dimension missmatch");
    }
    C->rows = A->rows;
    C->cols = A->cols;
    for (r = 0; r < C->rows; r++) {
        for (c = 0; c < C->cols; c++) {
            mat_set(C, r, c, mat_get(A, r, c) + mat_get(B, r, c));
        }
    }
}

/*
 Matrix Substraction
 Substracts matrixes A - B and stores the result in matrix C

 requires
      dimensions of A and B match: A.rows == B.rows && A.cols == B.cols
      Enough space has already been allocated for C (will not malloc())
*/
void mat_sub(const matrix_t * A, const matrix_t * B, matrix_t * C)
{
    size_t r,c;
    if (A->rows != B->rows || A->cols != B->cols) {
        ERROR("Matrix dimension missmatch");
    }
    C->rows = A->rows;
    C->cols = A->cols;
    for (r = 0; r < C->rows; r++) {
        for (c = 0; c < C->cols; c++) {
            mat_set(C, r, c, mat_get(A, r, c) - mat_get(B, r, c));
        }
    }
}

/*
  Polinomial matrix expansion
  Creates the grade <grade> polinomial expansion of input (n x 1)
  input:
        n x 1 matrix, grade
*/
matrix_t * expand_matrix(const matrix_t * input, const unsigned int grade) 
{
    if (mat_col(input) != 1) {
        ERROR("Polynomial expansion requires a column vector as input");
    }
    matrix_t * target = mat_create(mat_row(input), grade + 1);
    size_t r,c;
    for (r = 0; r < mat_row(target); r++) {
        mat_set(target, r, 0, 1); // Fist column is 1 (x^0)
        for (c = 1; c < mat_col(target); c++) {
            mat_set(target, r, c, 
                    pow(mat_get(input, r, 0), c));
        }
    }
    return target;
}
