#ifndef _BLOCK_H
#define _BLOCK_H
/*
 A block represents the contiguous block of data 
 Why do we need it? 
 There are many matrixes that essentially have the same data but are accessed differently, for example A and transpose(A).
 Another layer of indirection allows us to share the underlying data thus improving performance. 
 If we are sharing blocks, we need them to count references
*/

#include <stdlib.h>

typedef double TYPE; 

typedef struct {
    TYPE * data;
    size_t size;        // Size of the block: in number of TYPES
    unsigned int refcount;
} block_t;

// Allocators:
block_t * block_alloc(size_t size);
block_t * block_copy(const block_t* src);
void block_realloc(block_t * block, size_t newsize);

/*
   Note that block_free is not exposed: The memory will be free()ed when the last reference is removed
*/
block_t * block_add_ref(block_t * block);
void block_rem_ref(block_t * block);

// Getters / Setters
TYPE block_get(const block_t * b, size_t index);
void block_set(block_t * b, size_t index, const TYPE value);

#endif
