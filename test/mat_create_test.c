#include "error.h"
//#include "../src/matrix.h"
#include "matrix.h"

#include <stdio.h>

int mat_realloc()
{
    int r,c;
    matrix_t* count = mat_create(10,10);
    for (r = 0; r < 10; r++) {
        for (c = 0; c < 10; c++) {
            mat_set(count, r, c, r + 10 * c);
        }
    }
    mat_print_name(count, "Count");
    // Resize without data loss. from 10x10 to 50x2
    mat_resize(count, 50, 2);
    mat_print_name(count, "Count");

    // Resize to a bigger matrix
    mat_resize(count, 100, 2);
    mat_print_name(count, "Count");
   // 
   // // Resize back to the small one
    mat_resize(count, 50, 2);
    mat_print_name(count, "Count");
    mat_free(count);

    return 0;
}
int mat_insert()
{
    printf("Matrix insertion testing");
    double array[4][3] = {
          {  1,  2, 3 },
          {  4,  5, 6 },
          {  7,  8, 9 },
          { 10, 11, 12}
          };

    double sub_array[1][3] = {
          { 99, 99, 99}};
   
    double sub_array2[4][1] = {
          { 100},
          { 100},
          { 100},
          { 100}
          };
    double array_sol1[4][3] = {
          { 99, 99, 99},
          {  1,  2, 3 },
          {  4,  5, 6 },
          {  7,  8, 9 }
          };

    double array_sol2[4][3] = {
          { 100,  99, 99},
          { 100,  1,  2,},
          { 100,  4,  5,},
          { 100,  7,  8,}
          };

   
    matrix_t* A = mat_array(4, 3, array);
    matrix_t * Asol1 = mat_array(4, 3, array_sol1);
    matrix_t * Asol2 = mat_array(4, 3, array_sol2);
    matrix_t * row1 = mat_array(1, 3, sub_array); 
    matrix_t * col1= mat_array(4, 1, sub_array2); 

    mat_print_name(A, "A");
    mat_insert_row(A, row1, 0);
    mat_print_name(A, "A after insertion");
    if (!mat_equal(A, Asol1)) {
        ERROR("Insertion failed");
    }
    
    mat_print_name(A, "A");
    mat_insert_col(A, col1, 0);
    mat_print_name(A, "A after insertion");
    if (!mat_equal(A, Asol2)) {
        ERROR("Insertion failed");
    }

    mat_free(A);
    mat_free(Asol1);
    mat_free(Asol2);
    mat_free(row1);
    
    return 0;
}
int submatrix()
{
    printf("Submatrix testing\n");
    double array[4][3] = {
          {  1,  2, 3 },
          {  4,  5, 6 },
          {  7,  8, 9 },
          { 10, 11, 12}
          };

    double sub_array[4][1] = {
          { 2},
          { 5},
          { 8},
          {11}
          };
    matrix_t* A = mat_array(4, 3, array);
    matrix_t * row1 = mat_array(4, 1, sub_array); 
    // Second column, i.e Col 1 of A
    matrix_t * sub_A = mat_ROsubmatrix(A, 0, 3, 1, 1);
    matrix_t * sub_B = mat_create(2,2);

    mat_print_name(A, "A");
    mat_print_name(sub_A, "sub_A");
     
    if (!mat_equal(sub_A, row1))
        ERROR("SUBMATRIX DOESNT WORK OOOO");

    mat_ROsubmatrix_to(A, sub_B, 1 , 1, 0, mat_col(A) - 1);
    mat_print_name(sub_B, "Row 1 of A");
      
    mat_ROsubmatrix_to(A, sub_B, 1 , 3, 1, mat_col(A) - 1);
    mat_free(A);
    mat_print_name(sub_B, "Rows 1-3 cols 1-2 of A");

    printf("A freed\n");
    mat_print_name(sub_A, "sub_A"); // Legal. Valgrind should be happy
    mat_free(sub_A);
    mat_free(sub_B);
    mat_free(row1);
    
    return 0;

}
int transpose()
{
    double array[4][3] = {
          {  1,  2, 3 },
          {  4,  5, 6 },
          {  7,  8, 9 },
          { 10, 11, 12}
          };
    matrix_t* A = mat_array(4, 3, array);
    mat_print_name(A, "A");
    matrix_t* At = mat_ROtranspose(A);
    mat_print_name(At, "At");
    
    mat_free(A);
    mat_print_name(At, "At");

    mat_free(At);
    return 0;
}

int main(void)
{
    printf("Testing Matix creation\n");
    double array[3][3] = {
          { 1, 2, 3 },
          { 4, 5, 6 },
          { 7, 8, 9 }
          };
    
    matrix_t* mat = mat_array(3, 3, array); 
    
    printf("Matrix initialization\n");
    size_t r,c;
    for (r = 0; r < 3; r++) {
        for (c = 0; c < 3; c++) {
            if (mat_get(mat, r, c) != array [r][c]) {
                ERROR("Matrix initialization from array error");
                break;
            }
        }
    }

    // C99 only (not ANSI C(C90))
    matrix_t* mat2 = mat_array(3, 3, (double[3][3]){{1, 2, 3},{4, 5, 6}, {7, 8, 9}});
    mat_print_name(mat , "Mat");
    mat_print_name(mat2, "Mat2");
    if (!mat_equal(mat, mat2)) 
        ERROR ("Matrix are not equal");
    
    mat_free(mat);
    mat_free(mat2);
    
    unsigned int N = 10;
    matrix_t* I = mat_eye(10);
    mat_print_name(I, "I"); 
    for (r = 0; r < N; r++) {
        for (c = 0; c < N; c++) {
            if (r == c) {
                if (mat_get(I, r, c) != 1) {
                    ERROR("Eye matrix doesn't look too good.");
                }
            } else {
                if (mat_get(I, r, c) != 0) {
                    ERROR("Eye matrix doesn't look too good.");
                }
            }
        }
    }
    
    matrix_t* I2 = mat_assign_to(I);
    for (r = 0; r < N; r++) {
        for (c = 0; c < N; c++) {
            if (r == c) {
                if (mat_get(I2, r, c) != 1) {
                    ERROR("Eye2 matrix doesn't look too good.");
                }
            } else {
                if (mat_get(I2, r, c) != 0) {
                    ERROR("Eye2 matrix doesn't look too good.");
                }
            }
        }
    }
    mat_print_name(I2,"I2");
    // Changing the value of I1 should get reflected in I2 
    for (r = 0; r < N; r++) {
        for (c = 0; c < N; c++) {
            if (r == c) {
                mat_set(I, r, c, 2);
            }
        }
    }
    if (!mat_equal(I, I2)) {
        ERROR("Assigned matrixes not equal");
    }
    mat_print_name(I2,"I2");
    mat_free(I);
    mat_print_name(I2,"I2");
    // We still got I2 right?
    for (r = 0; r < N; r++) {
        for (c = 0; c < N; c++) {
            if (r == c) {
                if (mat_get(I2, r, c) != 2) {
                    ERROR("Eye2 matrix doesn't look too good.");
                }
            } else {
                if (mat_get(I2, r, c) != 0) {
                    ERROR("Eye2 matrix doesn't look too good.");
                }
            }
        }
    }
    mat_free(I2);
    matrix_t* count = mat_create(10,10);
    for (r = 0; r < 10; r++) {
        for (c = 0; c < 10; c++) {
            mat_set(count, r, c, r + 10 * c);
        }
    }
    matrix_t* count2 = mat_copy(count);

    if (!mat_equal(count, count2)) {
        ERROR("Copyed matrixes not equal");
    }
    
    mat_print_name(count, "Count");
    matrix_t * Ctrans;
    Ctrans = mat_transpose(count);
    mat_print_name(Ctrans, "Count Transpose");

    mat_free(count);
    mat_free(count2);
    mat_free(Ctrans);
    
    int rc = mat_realloc();
        
    rc = transpose();

    rc = submatrix();

    rc = mat_insert();
    return rc;
}

