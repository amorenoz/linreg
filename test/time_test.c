
#include <timing.h>
#include <stdio.h>

int example1()
{
    int i;
    int value = -8000;
    for (i = 0; i < 80000000; ++i) {
        value ++;
    }
    return value;
}

int example2(int param)
{
    int i;
    int value = -8000;
    for (i = 0; i < 80000000; ++i) {
        value ++;
    }
    return (value == 5) ? value: param;
}
int main()
{
    measure_t measure;
    int rc = time_fn(&example1,&measure);
    printf("Example1 returned %d \n", rc);
    print_measure("Example1",&measure);

    int rc2 = time_fn(&example2, &measure);
    printf("Example2 returned %d \n", rc2);
    print_measure("Example2",&measure);

    return 0;
}

