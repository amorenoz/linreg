#ifndef _TIME_H_
#define _TIME_H_
/*
 Time module: Provide useful functions for:
        Timestamping
        Time stream formatting
        Time measuring
*/

#define MAX_TIMESTAMP 20

typedef struct {
    int time;       // Number of total seconds
    double clock;   // Processing time (also in s)
} measure_t;

typedef unsigned long mytime_t; 


/*
 Measure the time of a given function
*/
int time_fn( int (*fp)(), measure_t * measure);
void print_measure(const char* description, const measure_t * m);

/*
  Convert mytime_t to and from strings
  Format of timestamp is: 2008-01-05 18:00
*/
void time_string(const mytime_t time, char* time_str);

mytime_t parse_time(const char* time_str);

mytime_t get_time(const unsigned short year, 
                const unsigned short month,
                const unsigned short day,
                const unsigned short hour,
                const unsigned short min);


#endif //_TIME_H_
