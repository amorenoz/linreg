#ifndef _LINEQ_H
#define _LINEQ_H
/* System of equations solvers */
#include "mat/private_matrix.h"
#include "error.h"



/* Solver identifier */
typedef enum{
	LUNP,
	CHOL
}leq_solver;

/* lin_solve. Solve 
  The idea is to introduce a generic Ax = b and then
  choose if we use LU, QR, Cholesky or iterative methods
*/
void lin_solve(const matrix_t * A, const matrix_t * b, 
	matrix_t * x, leq_solver type);

/* lu_nopivot
    Computes the lower triangular and upper triangular matrices
*/
void lu_nopivot(const matrix_t * A, matrix_t ** L, matrix_t ** U);

/* 
   Back substitution Ux = b
*/
void b_subst(const matrix_t * U, const matrix_t * b, matrix_t * x);

/*
   Forward substitution Lx = b
*/
void f_subst(const matrix_t *L, const matrix_t *b, matrix_t * x);

/* Cholesky factorization A*A = R*R
   requires:
        A must be hermitian positive definite matrix
*/
matrix_t * chol(const matrix_t * A);

#endif
