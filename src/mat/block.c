#include "block.h"
#include "error.h"

#include <stdio.h>
#include <string.h>

block_t * block_alloc(size_t size)
{

    // First we allocate the block structure itself
    block_t * block = malloc(sizeof(block_t));
    if (block == NULL) {
        ERROR("Failed to allocate block structure");
    }
    
    // Now, allocate the data block
    TYPE * data = calloc(size, sizeof(TYPE));
    if (data == NULL) {
        ERROR("Failed to allocate data block");
    }
    
    block->size = size;
    block->refcount = 1;
    block->data = data;
    return block;
}

void block_realloc(block_t * block, size_t newsize)
{
    
    TYPE * newdata = realloc(block->data, newsize * sizeof(TYPE));
    if (newdata == NULL) {
        ERROR("Failed to reallocate block");
    }
    
    if (block->size < newsize) {
        unsigned int i;
        for (i = block->size; i < newsize; i++) {
            newdata[i] = (TYPE)0;
        }
    }
    block->data = newdata;
    block->size = newsize;
}

void block_free(block_t * block)
{
    if (--(block->refcount) == 0) {
        // Free data block
        free(block->data);
        // Free block structure
        free(block);
    } else {
//        printf("%d references left, not free()ing\n", block->refcount);
    }
}

/*
   Reference counting
*/
block_t * block_add_ref(block_t * block)
{
    block->refcount++;
 //   printf("Add reference to block %p. It has now %d references \n", block, block->refcount);
    return block;
}

void block_rem_ref(block_t * block)
{
    if (block->refcount == 1)
    {
 //       printf("Removing last reference to block %p. Free()ing.\n", block);
        block_free(block);
    } else {
        block->refcount--;
        //printf("Removed reference to block %p. It has now %d references \n", block, block->refcount);
    }
}

// Getters / Setters
TYPE block_get(const block_t * b, size_t index) {
    return ((TYPE*) b->data)[index];
}

void block_set(block_t * b, size_t index, const TYPE value) {
    if (b->refcount > 1)
        printf("Warning: Changing block with more than one reference!\n");
    ((TYPE*)b->data)[index] = value;
}

// Copy the memory block to another block
block_t * block_copy(const block_t* src) {
    block_t * dst;
    dst = block_alloc(src->size);
    if (dst == NULL) {
        ERROR("Failed to allocate destination block");
    }
    //printf("memcopying dst: %p, src: %p size: %u\n", dst->data, src->data,(unsigned int) (dst->size));
    memcpy(dst->data, src->data, dst->size * sizeof(TYPE));
    return dst;
}
