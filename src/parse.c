// File Parser.
// Format:
//
#include "parse.h"

#include "mat/private_matrix.h"
#include "timing.h"

#include <stdio.h>
#include <string.h>

#define DEF_MAT_SIZE 10
#define MAXLINE 40

const char * obs_keyword = "observaciones";
const char * pred_keyword = "predicciones";

/*
   determine_nvars:
     Preparing for multi-variable linear regression problems:
     Determine the number of IV present in an observation line
     2008-01-25 10:00 1100 1200 1300 ... 8
     The last variable is considered the DV  

*/
unsigned int determine_nvars(char* line)
{
    unsigned int MAX_VARS = 100;
    unsigned int var;
    char *array[MAX_VARS];
    // Copy line into local variable   
    char myline[MAXLINE];
    strncpy(myline, line, MAXLINE);
    
    array[0] = strtok(myline, " ");
    if (array[0] == NULL) {
        //printf("Cannot determine the number of variables in: %s", line);
        return 0;
    }

    for (var = 0; var < MAX_VARS; var++) {
        array[var] = strtok(NULL, " ");
        if (array[var] == NULL) {
            //printf("End of line. Vars = %d", var);
            break;
        } else {
            //printf("Next var: %s", array[var]);
        }
    }
    return (var - 1); // The fist counted variable is the time (12:00)
}

/*
    find_insertion_point
    Determine the position to insert <time> in <time_matrix> in ascending order.
*/
size_t find_insertion_point(const matrix_t * time_matrix, 
                            const mytime_t time, 
                            const size_t start)
{
    size_t pos = start;
    while (pos != 0 && time < mat_get(time_matrix, pos -1, 0)) {
        pos --;
    }
    return pos;
}
/* 
    Insert a new prediction sorted by the timestamp
*/
void insert_sorted_pred(matrix_t * iv, matrix_t * pred_timestamp, 
                       const matrix_t * iv_row, 
                       mytime_t timestamp, size_t first_empty_field)
{
    size_t pos = find_insertion_point(pred_timestamp, timestamp, 
                                      first_empty_field);
    
    //printf("Inserting %ld to position %d \n", timestamp,(int) pos);
    matrix_t * onebyone = mat_create(1 , 1);
    // Insert iv row
    mat_insert_row(iv, iv_row, pos);

    // Insert timestamp
    mat_set(onebyone, 0, 0, timestamp); 
    mat_insert_row(pred_timestamp, onebyone, pos);

    mat_free(onebyone);
}
/* 
    Insert a new observation sorted by the timestamp
*/
void insert_sorted_obs(matrix_t * iv, matrix_t * dv, 
                       matrix_t * obs_timestamp, 
                       const matrix_t * iv_row, 
                       double value,
                       mytime_t timestamp,
                       const size_t first_empty_field)
{
    size_t pos = find_insertion_point(obs_timestamp, timestamp, first_empty_field);
    matrix_t * onebyone = mat_create(1 , 1);
    // Insert iv row
    mat_insert_row(iv, iv_row, pos);

    // Insert dv value
    mat_set(onebyone, 0, 0, value); 
    mat_insert_row(dv, onebyone, pos);
    
    // Insert timestamp
    mat_set(onebyone, 0, 0, timestamp); 
    mat_insert_row(obs_timestamp, onebyone, pos);

    mat_free(onebyone);
}

/*
    parse_line
    Retrieve timestamp, DV value and IV_row from a line
    If the line-type is PREDICTION value will not be assigned.

*/
int parse_line(char *line, const linetype_t type,
                           matrix_t * IV_row, 
                           double * value,
                           mytime_t * timestamp)
{
    char timestamp_str[40];
    char* value_str;
    unsigned int var; 
    // Copy line into local variable   
    char myline[MAXLINE];
    strncpy(myline, line, MAXLINE);

    /*The first two elements of the line are the timestamp:
     * 2008-01-05 18:00 1677 57.69
     */
    char * timestamp_begin = strtok(line, " ");
    if (timestamp_begin == NULL) {
        //printf("Error reading timestamp from %s \n", line);
        return -1;
    }
    strncpy(timestamp_str, timestamp_begin, MAX_TIMESTAMP);
    strncat(timestamp_str, " ",  MAX_TIMESTAMP);
    strncat(timestamp_str, strtok(NULL, " "), MAX_TIMESTAMP);
    
    *timestamp = parse_time(timestamp_str);
    
    // If it's an observation, the IV comes next
    if (type == OBSERVATION) {
        value_str = strtok(NULL, " ");
        *value = atof(value_str);
    }

    for (var = 0; var < IV_row->cols; var++) {
        char* value_str = strtok(NULL, " ");
        if (value_str == NULL) {
            printf("Insuficient variables\n");
            return -1; 
        } 
        mat_set(IV_row, 0, var, atof(value_str));
    }
    
    return 0;
}
/*
    parse_file.
    Populates the matrixes inside linreg_prob_t sorted by timestamp

*/
int parse_file(const char* filename, linreg_prob_t * problem) {

    FILE *fp;
    char line[MAX_LINE];

    int ret_code = 0;
    unsigned int nobs = 0;
    unsigned int npreds = 0;
    double value;
    mytime_t timestamp;
    unsigned int nvariables = 0;
    linetype_t linetype = UNDEFINED;
    
    if ((fp = fopen(filename, "r")) == NULL) {
        perror("Error opening file");
        return 1;
    }
    // Declaration / Initialization of Matrixes
    matrix_t *iv, *iv_row, *pred_X;
    matrix_t * dv = mat_zero(DEF_MAT_SIZE, 1); // Dependent variable
    matrix_t * pred_timestamp = mat_zero(DEF_MAT_SIZE, 1);
    matrix_t * obs_timestamp = mat_zero(DEF_MAT_SIZE, 1);
    
    while(fgets(line, MAX_LINE, fp) != NULL) {

        if (strncmp(line, obs_keyword, strlen(obs_keyword)) == 0) {
            //printf("observation mode \n");
            linetype = OBSERVATION;
            continue;
        }
        if (strncmp(line, pred_keyword, strlen(pred_keyword)) == 0) {
            //printf("prediction mode \n");
            linetype = PREDICTION;
            continue;
        }

        if (nvariables == 0) {
            nvariables = determine_nvars(line);
            if (nvariables < 2) {
                printf("Too few variables in the specified file. \n At least two variables are needed for observations.\n");
                return -1;
            }
            // The following assumption is made: Observations come fist.
            // TODO: Make it independent of the order of data
            // Now we know how many variables we have, we can allocate our
            // matrixes:
            iv = mat_zero(DEF_MAT_SIZE, nvariables - 1);
            iv_row = mat_zero(1, nvariables - 1);
            pred_X = mat_zero(1, nvariables - 1);
        }
        
        if (parse_line(line, linetype, iv_row, &value, &timestamp) != 0) {
            printf("Error parsing line: %s. Exiting\n", line);
            ret_code = -1;
            break;
        }
        // Add new observation
        if (linetype == OBSERVATION) {
            insert_sorted_obs(iv, dv, obs_timestamp, iv_row, value, timestamp, nobs);
            
            // Dynamically increase the size of matrixes
            if (nobs++ == dv->rows -1) {
                mat_resize(dv, dv->rows + DEF_MAT_SIZE, nvariables -1);
                mat_resize(iv, iv->rows + DEF_MAT_SIZE, 1);
                mat_resize(obs_timestamp, obs_timestamp->rows + DEF_MAT_SIZE, nvariables -1);
            }
        // Add new prediction
        } else if (linetype == PREDICTION) {
            insert_sorted_pred(pred_X, pred_timestamp, iv_row, timestamp, npreds);
            
            // Dynamically increase the size of matrixes
            if (npreds++ == pred_X->rows -1) {
                mat_resize(pred_X, pred_X->rows + DEF_MAT_SIZE, nvariables -1);
                mat_resize(pred_timestamp, pred_timestamp->rows + DEF_MAT_SIZE, nvariables -1);
            }
        }
    }
    // Fit to size
    mat_resize(dv, nobs, nvariables - 1); 
    mat_resize(iv, nobs, 1); 
    mat_resize(obs_timestamp, nobs, 1);
    mat_resize(pred_X, npreds, nvariables - 1); 
    mat_resize(pred_timestamp, npreds, 1);
    
    // Free auxiliary variables
    mat_free(iv_row);
    
    // Output assignment
    problem->DV = dv;
    problem->IV = iv;
    problem->predict_X = pred_X;
    problem->pred_timestamp = pred_timestamp;
    problem->obs_timestamp = obs_timestamp;
    
    return ret_code;
}

