
#include "lineq.h"
#include "matrix.h"

int main(void)
{
    double array[4][4] = {{18, 22,  54,  42},
                          {22, 70,  86,  62 },
                          { 54, 86, 174, 134},
                          {42, 62, 134, 106}};

    matrix_t * A = mat_array(4, 4, array);
    
    matrix_t * R = chol(A);
    matrix_t * Rt = mat_transpose(R);
    mat_print_name(R, "R");
    matrix_t * RR = mat_mult(Rt, R);
    mat_print_name(RR, "Rr");
    
    TYPE diff = 0;
    int r,c;
    for (r = 0; r < 4; r++) {
        for (c = 0; c < 4; c++) {
            diff+= (mat_get(RR,r,c) - mat_get(A,r,c));
        }
    }
    if (diff >= 0.0000001) {// Accurate enough?
        printf("Cholesky is not working.\n");
        return 1;
    }
    return 0;
}
