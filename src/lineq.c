#include "lineq.h"

#include <math.h>
/* Note: there are several checks that we must do for each of these
methods. For instance, in every one of those we have to check that
the system Ax = b doesn't have zero pivots. For Cholesky decomposition
A must be positive definite. Where should we asses this conditions
without hindering performance?
*/

/* lin_solve:
  The idea is to introduce a generic Ax = b and then
  choose if we use LU, QR, Cholesky or iterative methods
*/

void lin_solve(const matrix_t * A, const matrix_t * b, 
    matrix_t * x, leq_solver type)
{
    size_t m = A->rows;
    size_t n = b->rows;

    if(n != m) ERROR("System Ax = b must be determined");

    
    switch (type){

        case LUNP:
        {
            /* Routine for LU solve:
               Ax = b -> LUx = b -> L(Ux) = b -> Ly = b -> Ux = y 
            */

            // declare auxiliary variables
            matrix_t * L = NULL;
            matrix_t * U = NULL;
            matrix_t * y = mat_create(A->cols, b->cols);
            lu_nopivot(A, &L, &U);
            f_subst(L, b, y); 
            b_subst(U, y, x);

            mat_free(y);
            mat_free(L);
            mat_free(U);

            break;
        }

        case CHOL:
        {

            /* Routine for CHOL solve:
               Ax = b -> RtRx = b -> Rt(R)x = b -> Rty = b -> Rx = y 
            */

            matrix_t * R  = chol(A);
            matrix_t * Rt = mat_ROtranspose(R);
            
            /*
             * Solve lower-triangular system
             * Rt*w = b for w
             * */

            matrix_t * y = mat_create(mat_row(Rt), 1);
            f_subst(Rt, b, y);

            /*
             * Solve upper-triangular system
             * Rx = w for x
             * */
             
            b_subst(R, y, x);    
        }

        default:
        {
            ERROR("Incorrect solver type");
            break;
        }


    }

}

/* lu_nopivot
    Computes the lower triangular and upper triangular matrices
*/
void lu_nopivot(const matrix_t * A, matrix_t ** L, matrix_t ** U)
{
    size_t k, j, l;

    if(A->rows != A->cols) ERROR("Matrix must be square");
    
    /* L starts as eye and U as A, this can be optimized */
    size_t m = A->rows;
    *L = mat_eye(m);
    *U = mat_copy(A);

    for(k = 0; k <= m - 2; k++){
        for(j = k + 1; j <= m - 1; j++) {
            mat_set(*L, j, k, mat_get(*U, j, k)/mat_get(*U, k, k));
            for(l = k; l <= m - 1; l++) {
                mat_set(*U, j, l, mat_get(*U, j, l) -
                        mat_get(*L, j, k)*mat_get(*U, k, l));
            }
        }
    }
}

/*
   Back substitution Ux = b
*/
void b_subst(const matrix_t *U, const matrix_t *b, matrix_t * x)
{
    int i, j;
    int m = U->rows;

    for(i = m - 1 ; i >= 0; i--) {
        mat_set(x, i, 0, mat_get(b, i, 0));

        for(j = i + 1; j < m; j++) {
            mat_set(x, i, 0, 
                    mat_get(x, i, 0) -
                    mat_get(x, j, 0) * mat_get(U, i, j));
        }
        mat_set(x, i, 0, mat_get(x, i, 0) / mat_get(U, i, i));  
    }
}


/*
   Forward substitution Lx = b
*/
void f_subst(const matrix_t *L, const matrix_t *b, matrix_t * x) 
{
    size_t i, j;
    size_t m = L->rows;

    for(i = 0; i < m; i++) {
        mat_set(x, i, 0, mat_get(b, i, 0));
        for(j = 0; j < i; j++) {
            mat_set(x, i, 0, 
                    mat_get(x, i, 0) -
                    mat_get(x, j, 0) * mat_get(L, i, j));
        }
        mat_set(x, i, 0, mat_get(x, i, 0)/mat_get(L, i, i));  
    }
}

/* Cholesky factorization A*A = R*R
   requires:
        A must be hermitian positive definite matrix
*/
matrix_t * chol(const matrix_t * A) { 


    /*
     * Cholesky factorization:
     * A*A = R*R
     * */

    size_t j_to_m,j,k,k_to_m;
    if (A->rows != A->cols)
        ERROR("Matix must be square");

    /**
     * TODO: Check for definite-positive
     */
    size_t m = A->rows;
    matrix_t * R = mat_copy(A);
    TYPE Rkk;

    //Clear lower diagonal
    for(k = 0; k < m; k ++) {
        for (j = 0; j < k; j++) {
            mat_set(R, k, j, 0);
        }
    }
    
    /* Cholesky algorithm: Source:
       Numerical Linear Algebraumerical Linear Algebra
       by Lloyd N. Trefethen et al
    */
    for (k = 0; k < m ; k++) {
        for (j = k + 1; j < m; j++) {
            TYPE aux = fabs(mat_get(R, k, j)) / mat_get(R, k, k);
            for(j_to_m = j; j_to_m < m; j_to_m++) {
                mat_set(R, j, j_to_m, 
                        mat_get(R, j, j_to_m) -
                            mat_get(R, k , j_to_m) * aux);
            }
        }
        Rkk = mat_get(R, k, k); 
        for (k_to_m = k; k_to_m < m; k_to_m++) {
            mat_set(R, k, k_to_m, mat_get(R, k, k_to_m) / sqrt(Rkk));
        }
    }
    return R;
}
