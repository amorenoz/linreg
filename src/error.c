#include "error.h"

#include <stdio.h>
#include <stdlib.h>

void error (const char * reason, const char * file, int line)
{
    fprintf(stderr, "%s [%d]: %s.\n", file, line, reason);
    fflush(stderr);
    exit(-1);
}
