#include "probs.h"
#include "lineq.h"

#include <math.h>
#include <stdio.h>

/*
    Print the matrixes of a Linear Regression problem
*/
void prob_print(linreg_prob_t * prob)
{
    printf("Linear Regression Problem: \n");
    mat_print_name(prob->IV, "Independent variable matrix (IV)");
    mat_print_name(prob->DV, "Dependent variable matrix (DV)");
    mat_print_name(prob->predict_X, "Prediction X");
    mat_print_name(prob->coef, "Coeficients"); 
    mat_print_name(prob->predict_Y, "Prediccion Y"); 

}
/*
    Estimate: Given a coeficient vector and an input vector, generates an estimation vector

    input: coeficient vector (column)
           input matrix. If a column vector is provided, it will be expanded polynomically to match the grade (infered from the length of the coeficients
  output: column vector of estimations
*/
matrix_t * estimate(const matrix_t * coef, const matrix_t * input)
{
    if (mat_col(coef) > 1) {
        ERROR("Coeficients must be a column vector");
    }
    
    size_t grade = mat_row(coef) - 1;
    size_t n_pred= mat_row(input);
    int polynomial = 0;
    unsigned int pred, c;
    const matrix_t * predX; 
    matrix_t * expandedX;

    if(mat_col(input) != 1) {
        // Multivariable
        if (mat_row(coef) != mat_col(input)) {
            ERROR("The dimensions of the coeficients and the input data do not match");
        }
        predX = input;
    } else {
        // Polynomial
        polynomial = 1;
        expandedX = expand_matrix(input, grade);
        predX = expandedX;
    }

    matrix_t * estimation = mat_create(mat_row(input), mat_col(input));
    
    // Temporary matrixes
    matrix_t * sol = mat_create(1, 1);
    matrix_t * coefTrans = mat_ROtranspose(coef);
    matrix_t * Xi = mat_create(grade + 1, 1);
    
    for(pred = 0; pred < n_pred; pred++) {
        for (c = 0; c < (grade + 1); c ++) {
            mat_set(Xi, c, 0, mat_get(predX, pred, c));
        }
        mat_mult_to(coefTrans, Xi, sol);
        mat_set(estimation, pred, 0, mat_get(sol, 0, 0));
    }

    mat_free(sol);
    mat_free(coefTrans);
    mat_free(Xi);
    if (polynomial == 1) mat_free(expandedX);

    return estimation;
}

/*
    Calculate average errror of a range of observations
*/
double average_error(linreg_prob_t * prob, unsigned int from, unsigned int to)
{
    unsigned int i;
    if (prob->coef == NULL) {
        ERROR ("Problem is not solved. Solve it before calculating errors");
    }
    if (from > mat_row(prob->IV) || to > mat_row(prob->IV)) {
        ERROR ("Incorrect range");
    }
    double sum = 0;
    int N = to - from + 1;

    matrix_t * IVsubvector = mat_ROsubmatrix(prob->IV, from, to, 0, 0);
    matrix_t * DVsubvector = mat_ROsubmatrix(prob->DV, from, to, 0, 0);
    matrix_t * estimation = estimate(prob->coef, IVsubvector);
    for (i = 0; i < mat_row(estimation); i++) {
        sum += fabs(mat_get(estimation, i, 0) - mat_get(DVsubvector, i, 0));
    }
    mat_free(IVsubvector);
    mat_free(DVsubvector);
    mat_free(estimation);

    return sum / N;
}

/*
    Solve Linear Regression Problem.
    The problem will be solved by Least Squares method using Cholesky decomposition.
    input: A problem representation
           Grade of polynomial expansion to apply

*/
int solve_linreg(linreg_prob_t * prob, unsigned int grade)
{
    // TODO: 
    // if (!valid_reg_data(prob)) {
    //    printf("Invalid data\n");
    //    return 1;
    //}
   
    // Variable initialization
    matrix_t * coef = mat_create(grade + 1 , 1);
    matrix_t * b = mat_copy(prob->DV);
    matrix_t * A = expand_matrix(prob->IV, grade);
    matrix_t * At = mat_ROtranspose(A);
    
    matrix_t * AtA = mat_mult(At, A);
    matrix_t * Atb = mat_mult(At, b);

    
    /*
     * Cholesky factorization:
     * A*A = R*R
     * */ 
    matrix_t * R = chol(AtA);
    matrix_t * Rt = mat_ROtranspose(R);
    
    /*
     * Solve lower-triangular system
     * R*w = A*b for w
     * */
    matrix_t * w = mat_create(mat_row(Rt), 1);
    f_subst(Rt, Atb, w);
    /*
     * Solve upper-triangular system
     * Rx = w for x
     * */
    b_subst(R, w, coef);


    //lin_solve(AtA, Atb, coef, CHOL);
   
    // Free auxiliary variables
    mat_free(b);
    mat_free(R);
    mat_free(Rt);
    mat_free(w);
    mat_free(A);
    mat_free(Atb);
    mat_free(At);
    mat_free(AtA);

    /*
     * Yestimated = ThetaT * X
     * */
    prob->predict_Y = estimate(coef, prob->predict_X); 
    prob->coef = coef;
    
    return 0;  
}

